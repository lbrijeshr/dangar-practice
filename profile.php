<?php
require_once 'config.php';
if(!isset($_SESSION['user'])){
    header("Location: login.php");
}
$profile = $_SESSION['user'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
    <link 
        rel="stylesheet" 
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" 
    />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>
                    Welcome, <strong><?= $profile['email'] ?></strong>
                </h3>
            </div>
            <form action="logout.php" method="post">
                <input type="submit" name="logout_submit" value="Logout" class="btn btn-sm btn-danger" />
            </form>
        </div>
    </div>
</body>
</html>