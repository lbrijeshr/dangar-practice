<?php
  
$mall = [
    'bottom_wear' => [
        'ladies' => [
            'saree', 
            'kurti', 
            'tshirt'
        ],
        'jents' => [
            'buskot', 
            'patlun', 
            'lungi'
        ],
        'children' => [
            'jakit', 
            'shocks', 
            'gaoun'
        ],
    ],
    'top_waer' => [
        'ladies' => [
            'saree1', 
            'kurti1', 
            'tshirt1'
        ],
        'jents' => [
            'buskot1', 
            'patlun1', 
            'lungi1'
        ],
        'children' => [
            'jakit1', 
            'shocks1', 
            'gaoun1'
        ],
    ],
];
?>
<table>
    <thead>
        <tr>
            <th>Type</th>
            <th>Gender</th>
            <th>Name</th>
        </tr>
    </thead>
<?php
foreach ($mall as $k => $v) {
    foreach ($v as $k2 => $v2) {
        foreach ($v2 as $v3) {
        ?>
            <tr>
                <td><?= $k ?></td>
                <td><?= $k2 ?></td>
                <td><?= $v3 ?></td>
            </tr>
        <?php
        }
    }
}