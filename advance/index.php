<?php

// require 'class1.php';

class basicMaths {
    public $total = 2;

    public static $final;
    public static $_instance;
    
    public function addition($val1 = 0, $val2 = 0){
        $val1 = (float) $val1;
        $val2 = (float) $val2;
        $this->total = $val1 + $val2;
    }

    public function substract($val1 = 0){
        $val1 = (float) $val1;
        $this->total -= $val1;
    }
    public function add($val1 = 0){
        $val1 = (float) $val1;
        $this->total += $val1;
    }

    public static function s_addition($val1 = 0, $val2 = 0){
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        $val1 = (float) $val1;
        $val2 = (float) $val2;
        self::$final = $val1 + $val2;
        return self::$_instance;
    }

    public static function s_substract($val1 = 0){
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        $val1 = (float) $val1;
        self::$final -= $val1;
        return self::$_instance;
    }
    public static function s_add($val1 = 0){
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        $val1 = (float) $val1;
        self::$final += $val1;
        return self::$_instance;
    }
    public static function getFinal(){
        return self::$final;
    }

}

$clone = new basicMaths;
echo $clone->total."<br />";

$clone->addition(10,8);
echo "After Addition: " . $clone->total . "<br />"; //18

$clone->substract(5);
echo "After Substract: " . $clone->total . "<br />"; //13

$clone->add(80);
echo "After Add: " . $clone->total . "<br />"; // 93

$clone->addition(10,20);
echo "After Addition: " . $clone->total . "<br />"; //30

$clone->substract(50);
echo "After Substract2: " . $clone->total . "<br />"; // 43

echo basicMaths::s_substract(10)
->s_add(5)
->s_add(5)->s_addition(14)->s_substract(4)->getFinal();
