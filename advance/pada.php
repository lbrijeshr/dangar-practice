<?php

class pada {
    public $lower;
    public $higher;

    public $step;

    public $padas = [];

    public function generatePadas(int $l = 1, int $h = 10, int $s = 10){
        $this->lower = $l;
        $this->higher = $h;
        $this->step = $s;
        for($i = $this->lower; $i <= $this->higher; $i++){
            $padas = [];
            for($j = 1; $j <= $this->step; $j++){
                $padas[$j] = $i*$j;
            }
            $this->padas[$i] = $padas;
        }
    }
}

$clone = new pada;
$clone->generatePadas(1,10);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Padas</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" />
</head>
<body>
    <div class="container">
        <div class="row mt-5">
            
                <?php
                    foreach ($clone->padas as $i => $ij) {
                        ?>
                        <div class="col-3 d-flex justify-content-center">
                        <table class="table table-bordered"><tbody>
                            <?php
                            foreach ($ij as $j => $total) {
                                ?>
                                <tr>

                                    <td> <?= $i ?> X <?= $j ?> = <?= $total ?> </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody></table></div>
                        <?php
                    }
                ?>
            
        </div>
    </div>
</body>
</html>