<?php

require_once 'config.php';


if(isset($_POST['login_submit'])){
    $email = trim($_POST['email']);
    $password = trim($_POST['password']);

    $checkQuery = "
        SELECT 
            * 
        FROM 
            `users` 
        WHERE 
            `email` = '{$email}'
            AND `password` = '{$password}'
        LIMIT 1
    ";

    try {
        $result = mysqli_query($con, $checkQuery);
        if($result->num_rows > 0){
            $data = mysqli_fetch_assoc($result);
            $_SESSION['user'] = $data;
            header("Location: profile.php");
        }else{
            $_SESSION['login_error'] = "Email or password is incorrect!";
            header("Location: login.php");
        }
    } catch (\Throwable $th) {
        $_SESSION['login_error'] = $th->getMessage();
        header("Location: login.php");
    }

}

?>