<?php 

$hostname = 'localhost';
$username = 'root';
$password = '5700';
$database = 'dangar';

$conn = mysqli_connect($hostname, $username, $password, $database);

if(!$conn){
	echo "<h2>Connection_error</h2>";
} 
if($_GET['edit']){
    $sql_update = "SELECT * FROM student_record WHERE `id` = {$_GET['edit']}";
    $result_update = mysqli_query($conn , $sql_update);
    $update_row = mysqli_fetch_assoc($result_update);
}
if($_GET['delete']){
    $sql_update = "DELETE FROM student_record WHERE `id` = {$_GET['delete']}";
    $result_update = mysqli_query($conn , $sql_update);
    header("Location: db.php");
}

if(isset($_POST['submit'])){
    if(empty($_POST['id'])){
        $query = "
            INSERT INTO `student_record` 
                SET
            `firstname` = '{$_POST['firstname']}',
            `lastname` = '{$_POST['lastname']}',
            `address` = '{$_POST['address']}'
        ";
    }else{
        $query = "
            UPDATE `student_record` 
                SET
            `firstname` = '{$_POST['firstname']}',
            `lastname` = '{$_POST['lastname']}',
            `address` = '{$_POST['address']}'
            WHERE
                `id` = '{$_POST['id']}'
        ";
    }
    
    $result = mysqli_query($conn, $query);
    header("Location: db.php");
}

$sql = " SELECT * FROM student_record";
$result = mysqli_query($conn , $sql);
$data = [];
while($row = mysqli_fetch_assoc($result)){
	$data[] = $row;
}

?>

<!DOCTYPE html>
<html>
<head>
	<title> student_data_in_tabular_form_practice </title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet">
 	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="container">
		<div class="row mt-5">
            <div class="col-sm-8">
                <form method="post">
                    <input type="hidden" name="id" value="<?= isset($update_row) ? $update_row['id'] : '' ?>">
                    <div class="form-floating mb-3">
                        <input 
                            type="text" 
                            class="form-control" 
                            id="firstname" 
                            placeholder="First Name" 
                            name="firstname"
                            value="<?= isset($update_row) ? $update_row['firstname'] : '' ?>"
                        />
                        <label for="firstname">First Name</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input 
                            type="text" 
                            class="form-control" 
                            id="lastname" 
                            placeholder="Last Name" 
                            name="lastname"
                            value="<?= isset($update_row) ? $update_row['lastname'] : '' ?>"
                        />
                        <label for="lastname">Last Name</label>
                    </div>
                    <div class="form-floating mb-3">
                        <textarea name="address" class="form-control" id="address" rows="5" placeholder="Address"><?= isset($update_row) ? $update_row['address'] : '' ?></textarea>
                        <label for="address" class="form-label">Address</label>
                    </div>
                    <input type="submit" class="btn btn-success btn-lg" name="submit" value="Save">
                    <?php 
                        if($is_submmited){ 
                    ?>
                            <div class="alert alert-success mt-3 alert-dismissible fade show" role="alert">
                                <strong>Success!</strong> Record Added Successfully!
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>  
                    <?php
                        }
                    ?>
                </form>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12 table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($data as $student) {
                        ?>
                            <tr>
                                <td><?= $student['firstname'] ?></td>
                                <td><?= $student['lastname'] ?></td>
                                <td><?= $student['address'] ?></td>
                                <td>
                                    <a href="?edit=<?= $student['id'] ?>" class="btn btn-info btn-sm">Edit</a>
                                    <a href="?delete=<?= $student['id'] ?>" class="btn btn-danger btn-sm ml-2">Delete</a>
                                </td>
                            </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</body>
</html>