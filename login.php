<?php
require_once 'config.php';

if(isset($_SESSION['user'])){
    header("Location: profile.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link 
        rel="stylesheet" 
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" 
    />
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row d-flex justify-content-center align-items-center" style="height: 100vh">
                    <div class="col-6">
                        <form method="post" action="check_login.php">
                            <div class="mb-3">
                                <label for="emailInput" class="form-label">Email address</label>
                                <input type="email" name="email" class="form-control" id="emailInput" aria-describedby="emailHelp">
                            </div>
                            <div class="mb-3">
                                <label for="passwordInput" class="form-label">Password</label>
                                <input type="password" name="password" class="form-control" id="passwordInput">
                            </div>
                            <?php
                                if(isset($_SESSION['login_error'])){
                            ?>
                                <div class="mb-3">
                                    <div class="alert alert-danger" role="alert">
                                        <?=  $_SESSION['login_error'] ?>
                                    </div>
                                </div>
                            <?php
                                }
                                unset($_SESSION['login_error']);
                            ?>
                            <input type="submit" name="login_submit" value="Submit" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>