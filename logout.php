<?php

require_once 'config.php';

if(isset($_POST['logout_submit'])){
    if(isset($_SESSION['user'])){
        unset($_SESSION['user']);
    }
    header("Location: login.php");
}