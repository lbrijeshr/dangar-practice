<?php

const CATEGORIES = [
    [
        'id' => 1,
        'parent_id' => null,
        'name' => 'Main Cat',
        'extra_charge' => null
    ],
    [
        'id' => 2,
        'parent_id' => 1,
        'name' => 'Sub Cat',
        'extra_charge' => 85
    ],
    [
        'id' => 3,
        'parent_id' => 2,
        'name' => 'Sub Cat 2',
        'extra_charge' => 54
    ],
    [
        'id' => 4,
        'parent_id' => 3,
        'name' => 'Sub Cat 3',
        'extra_charge' => null
    ],
];

$product = [
    'id' => 1,
    'price' => 150,
    'category_id' => 4,
    'name' => 'MRF Bat'
];

$category_charge = findExtraChargeByCategoryId($product['category_id']);
$total = $category_charge + $product['price'];
echo "<h1>{$total}</h1> <h4>{$product['price']} + {$category_charge}</h4>";

// empty => return true if string is blank empty or 0
// is_null => return true if string null

function findExtraChargeByCategoryId($category_id){
    if(empty($category_id)){
        return 0;
    }
    
    foreach(CATEGORIES as $category){
        if($category['id'] == $category_id){
            if(!is_null($category['extra_charge'])){
                return $category['extra_charge'];
            }
            else if(!is_null($category['parent_id'])){
                return findExtraChargeByCategoryId($category['parent_id']);
            }
            else{
                return 0;
            }
        }
    }
    return 0;
}



